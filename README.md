# SNES Music Player

## History

This is a project I started back when I was in high school in 2008. Back then I
had found an emulator written in 32-bit X86 assembly for the SNES APU by Anomie
(https://www.alpha-ii.com/). I barely knew what I was doing, as I only had 1.5
CS classes in Java at the time under my belt. This project was my first foray
into undertanding the linking process of applications, and learning how the
SNES APU worked. I experimented with creating a CLI music player for SPC files,
and even began experimenting with a basic Qt4 UI.

As I didn't know what I was doing, it wasn't until later that I began to
(crappily) use source version control. By this point, however, I had mostly
lost some of my old code that handled a CLI vesrion of the music player.
However, I was able to dig up some old backups, and managed to recover some of
the code, which I'm incorporating in this repository. I recall that the
original CLI code was done using normal C (I remember using `system("clear")` to
fake refreshing), but eventually I moved to using ncurses.

Returning to this code 14 years later is quite a trip down memory lane, and
shows me how far I've come since then. I've decided to dig into this old code
again to refactor it. However, as most systems have moved away from 32-bit x86
programs, I am replacing Anomie's emulator with Blargg's.

Why am I reviving this project? Well, for kicks and giggles, and as an
experiment in rewriting really old code. This was all written in C++03, and I'm
definitely going to use at least C++20 now. Plus, I like working on SNES
related projects.

## Dependencies

I've backed up Blargg's snes_spc-0.9.0 emulator at
https://gitlab.com/gemarcano/snes_spc. I've additionally augmented the code to
add support for adjusting the pitch, and to allow building it as a library.
This project uses the snes_spc library.

Additionally, I've developed a SPC id666 and xid666 parsing library called
libspc_tag at https://gitlab.com/gemarcano/libspc_tag . I use this to parse the
data from the SPC files in the music player.

For more traditional dependencies, I use Qt5, libao, and ncurses.

## Building

Currently only the Qt test application can be built, but I'm working on getting
the console application hooked up to the project. To build the Qt application:
```
$ mkdir build
$ cd build
$ qmake5 ../
$ make
```

## License

In case I haven't yet added a LICENSE file, I am planning on dual-licensing my
code as LGPL-2.1 or later and GPL-2 or later. If someone really thinks this
should be something else, I'm open to a discussion about it, but I do prefer
copyleft for projects like these.
