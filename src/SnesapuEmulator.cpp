//***********************************************************************/
//***********************************************************************/
//****	Project: 	GEM Audio Player
//****	Programmer:	Gabriel E. Marcano
//****	Oper. Sys.:	General
//****	File Name:	SnesapuEmulator.cpp
//****	Date:		August 3, 2009
//***********************************************************************/
//***********************************************************************/
//***********************************************************************/

//***********************************************************************/
//***********************************************************************/
//****	ChangeLog
//****
//**** 	August 3, 2009:
//****    -Anyways, I scrapped the old code and began restructuring it
//****     to be more general. This part of the engine doesn't need to
//****     be tied to the operating system, just the compiled libs of
//****     the emulator.
//***********************************************************************/
//***********************************************************************/

//#define DEBUG
#include "SnesapuEmulator.h"

#include <snes_spc/spc.h>
#include <spc_tag/spc_tag.h>

#include <fstream>
#include <filesystem>

/***********************************************************************/
/****************  SnesapuEmulator Functions   *********************/
/***********************************************************************/

typedef unsigned long Hertz;

SnesapuEmulator::SnesapuEmulator()
:
	mSpcSpeed(1/1),
	mSpcPitch(0)
{
	spc = spc_tag_new();
	emu = spc_new();
}


SnesapuEmulator::~SnesapuEmulator()
{
	spc_delete(emu);
	spc_tag_free(spc);
}

//Load an SPC file into the emulator
//check if it is an spc file
//Parse data into mSpcData
//get emulator ready to play

int SnesapuEmulator::loadSPC(const std::string aSpcFileLocation)
{
	if (aSpcFileLocation.empty())
		return -1;

	// FIXME error checking
	std::uintmax_t filesize = std::filesystem::file_size(aSpcFileLocation);
	std::ifstream file(aSpcFileLocation, std::ios::binary);
	std::vector<unsigned char> spc_data(filesize);
	file.read(reinterpret_cast<char*>(spc_data.data()), filesize);
	file.close();
	spc_tag_parse_memory(spc_data.data(), spc_data.size(), spc);

	spc_load_spc(emu, spc_data.data(), spc_data.size());

	//NOTE:Keep fade length reasonable -- is this desirable? How about a
	//parameter?

	return 0;
}

//Function to begin emulating
std::vector<int16_t> SnesapuEmulator::EmulateOneSecond()
{
	//FIXME: Check if buf needs to be resized
	std::vector<int16_t> buf(32000*2);
	spc_play(emu, 32000*2, buf.data());
	return buf;
}

const spc_tag *SnesapuEmulator::get_spc_tag() const
{
	return spc;
}

#ifdef _TEST_


#include <iostream>
int main ()
{
SnesapuEmulator emu;
std::cout << emu.setSPC("./a.spc") << '\n';

for (int i = 0; i < 11; i++)
{
	std::cout << emu.mSpcData[spcfile_content(i)] << '\n';
}

return 0;
}

#endif //_TEST_
