
#include <Ncurses.h>
#include <iostream>
#include <string>
#include <climits>

//helper function

bool askYN(std::string mesg, Ncurses& sys)
{
	std::string buffer;
	while (true)
	{
		sys << mesg << "\n[Y,N]: ";
		sys >> buffer;
		if ((buffer != "Y") && (buffer != "N"))
		{
			sys << "Please type either Y or N.\n";
		}
		else
		{
			if (buffer == "Y") {return true;} else {return false;}
		}
	}
}

bool askYN(Ncurses& sys)
{
	std::string buffer;
	while (true)
	{
		sys << "[Y,N]: ";
		sys >> buffer;
		if ((buffer != "Y") && (buffer != "N"))
		{
			sys << "Please type either Y or N.\n";
		}
		else
		{
			if (buffer == "Y") {return true;} else {return false;}
		}
	}
}
