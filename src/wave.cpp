
#include <wave.h>
#include <Libao_control.h>
#include <SnesapuEmulator.h>

#include <string>
#include <cstdint>
#include <fstream>
#include <iostream>

WAVE::WAVE()
{
	riff="RIFF";
	//ChunkSize = 36 + length;
	format = "WAVE";
	SubChunk1ID = "fmt ";
	Subchunk1Size=16;
	AudioFormat=1;
	//NumChannels = Lib.valformat_ch();
	//SampleRate = Lib.valformat_rate();
	//ByteRate=(SampleRate * NumChannels*Lib.valformat_bit()/8);
	//BlockAlign=(NumChannels * valformat_bit()/8);
	//BitsPerSample = valformat_bit();; //8,16,32,64,...
	Subchunk2ID ="data";
	//Subchunk2size=length; //(numsamples * numchannels* BitsperSample/8);
}

int WAVE::makePCM(SnesapuEmulator& Emu)
{
	std::vector<int16_t> buf;
	const spc_tag *tag = Emu.get_spc_tag();
	uint32_t length = spc_tag_get_seconds_until_fade(tag);
	for (size_t i = 0; i < length; ++i)
	{
		auto tmp = Emu.EmulateOneSecond();
		buf.insert(buf.end(), tmp.begin(), tmp.end());
	}
	//open file for writing and write PCM data to it
	std::ofstream file;
	std::string bufs = spc_tag_get_game_title(tag);
	bufs += " ][ ";
	bufs += spc_tag_get_song_title(tag);
	bufs += ".raw";
	file.open(bufs, std::ios::binary | std::fstream::trunc);
	file.write(reinterpret_cast<char*>(buf.data()), buf.size()*2);
	file.close();

	return 0;
}

int WAVE::makeWAV(SnesapuEmulator& Emu, Libao_control& Lib)
{
	std::vector<int16_t> buf;
	const spc_tag *tag = Emu.get_spc_tag();
	uint32_t length = spc_tag_get_seconds_until_fade(tag);
	for (size_t i = 0; i < length; ++i)
	{
		auto tmp = Emu.EmulateOneSecond();
		buf.insert(buf.end(), tmp.begin(), tmp.end());
	}
	std::ofstream file;
	std::string bufs = spc_tag_get_game_title(tag);
	bufs += " ][ ";
	bufs += spc_tag_get_song_title(tag);
	bufs += ".wav";
	std::cout << bufs << '\n';
	file.open(bufs, std::fstream::binary | std::fstream::trunc);

	length = buf.size()*2;


	ChunkSize = 36 + length;
	NumChannels = Lib.valformat_ch();
	SampleRate = Lib.valformat_rate();
	ByteRate=(SampleRate * NumChannels*Lib.valformat_bit()/8);
	BlockAlign=(NumChannels * Lib.valformat_bit()/8);
	BitsPerSample = Lib.valformat_bit();; //8,16,32,64,...
	Subchunk2size=length; //(numsamples * numchannels* BitsperSample/8);
	//make header
	file.write(riff.data(), 4);
	file.write(reinterpret_cast<char*>(&ChunkSize),4);
	file.write(reinterpret_cast<char*>(format.data()),4);
	file.write(reinterpret_cast<char*>(SubChunk1ID.data()),4);
	file.write(reinterpret_cast<char*>(Subchunk1Size),4);
	file.write(reinterpret_cast<char*>(AudioFormat),2);
	file.write(reinterpret_cast<char*>(NumChannels),2);
	file.write(reinterpret_cast<char*>(SampleRate),4);
	file.write(reinterpret_cast<char*>(ByteRate),4);
	file.write(reinterpret_cast<char*>(BlockAlign),2);
	file.write(reinterpret_cast<char*>(BitsPerSample),2);
	file.write(reinterpret_cast<char*>(Subchunk2ID.data()),4);
	file.write(reinterpret_cast<char*>(Subchunk2size),4);
	file.write(reinterpret_cast<char*>(buf.data()), buf.size()*2);

	file.close();
	return 0;
}
