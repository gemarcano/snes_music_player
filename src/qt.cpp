#include <QtGui>
#include "qt.h"
#include "SnesapuEmulator.h"
#include "Libao_control.h"

#include <fstream>
#include <iostream>

SnesapuEmulator emu;
Libao_control ao;

GUI_TEST::GUI_TEST(QWidget* parent)
: QWidget(parent)
{
	QGridLayout* mainLayout = new QGridLayout;

	playButton = new QPushButton("Play");
	stopButton = new QPushButton("Stop");

	mainLayout->addWidget(playButton,0,0);
	mainLayout->addWidget(stopButton,0,1);

	setLayout(mainLayout);

	connect(playButton, SIGNAL(clicked()), this, SLOT(play()));
	connect(stopButton, SIGNAL(clicked()), this, SLOT(stop()));

}

bool shared_stop = false;

void play()
{
	const spc_tag * spc = emu.get_spc_tag();
	auto length = spc_tag_get_seconds_until_fade(spc);
	for(unsigned int i = 0; (i < length) && (!shared_stop); i++)
	{
		std::vector<int16_t> tmp = emu.EmulateOneSecond();
		ao.play(tmp);
	}
}



void GUI_TEST::play()
{
	shared_stop = false;
	std::cout << "Play\n";
	audio_thread = std::thread(::play);
}

void GUI_TEST::stop()
{
	std::cout << "Stop\n";
	shared_stop = true;
	audio_thread.join();
}

 int main(int argc, char *argv[])
 {
     QApplication app(argc, argv);

	  emu.loadSPC(std::string(argv[1]));
     //QWidget window;
	  GUI_TEST a;

     a.show();

     return app.exec();
 }
