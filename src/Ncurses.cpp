#define BACKSPACE 127
#include <Ncurses.h>
//I only need std::strings to work with here, so I'm only drawing that
#include <iostream>
#include <sstream>

Ncurses& Ncurses::get()
{
	static Ncurses instance;
	return instance;
}

Ncurses::Ncurses() //sets up the ncurses environment with defaults guided towards oposite of std cout and cin
{
	initscr();
	addWindow(0, 0, 0, 0);
}

Ncurses::~Ncurses()//releases resourses used by ncurses
{
	endwin();
}

		//*************************************
		//****	Printing Functions
		//*************************************
void Ncurses::print(int s)
{
	auto& window = windows[activeWin];
	window.print(s);
}

void Ncurses::print(std::string s)
{
	print(s.c_str());
}


void Ncurses::print(const char* s)
{
	auto& window = windows[activeWin];
	window.print(s);
}

void Ncurses::print(char s)
{
	auto& window = windows[activeWin];
	window.print(s);
}

void Ncurses::println(std::string s)
{
	auto& window = windows[activeWin];
	window.println(s);
}

void Ncurses::println(const char* s)
{
	auto& window = windows[activeWin];
	window.println(s);
}

void Ncurses::println(char s)
{
	auto& window = windows[activeWin];
	window.println(s);
}

		//*************************************
		//****	Formatting Activation Functions
		//*************************************
void Ncurses::switchUnderline()
{
	auto& window = windows[activeWin];
	window.switchUnderline();
}

void Ncurses::switchBold()
{
	auto& window = windows[activeWin];
	window.switchBold();
}
//		void switch();
//		void switch();
		//void setColor(int);

		//************************************
		//****	Informative functions
		//************************************
int Ncurses::getX(int w)
{
	return windows[w].get_cursor().first;
}


int Ncurses::getY(int w)
{
	return windows[w].get_cursor().second;
}

		//************************************
		//****	Window related functions
		//************************************
void Ncurses::draw()//all by default
{
	refresh();
	move(0, 0);
	for (auto& win : windows)
		win.draw();
}

nwindow& Ncurses::addWindow(int height, int width, int startx, int starty)
{
	windows.emplace_back(height, width, startx, starty);
	return windows.back();
}

void Ncurses::closeWindow(size_t w)
{
	windows.erase(windows.begin() + w);
}

		//************************************
		//****	Input Related Functions
		//************************************

std::string nwindow::getLine()
{
	std::string result;
	for (int ch = wgetch(window_); static_cast<char>(ch) != '\n'; ch = wgetch(window_))
	{
		result += static_cast<char>(ch);
	}
	return result;
}

int nwindow::getKey()
{
	return wgetch(window_);
}


std::string Ncurses::getLine()
{
	auto& window = windows[activeWin];
	return window.getLine();
}

int Ncurses::getKey()
{
	auto& window = windows[activeWin];
	return window.getKey();
}

Ncurses& operator<< (Ncurses& left, std::string& right)
{
	left.print(right);
	return left;
}

Ncurses& operator<< (Ncurses& left,const char* right)
{
	left.print(right);
	return left;
}

Ncurses& operator<< (Ncurses& left,char right)
{
	left.print(right);
	return left;
}

Ncurses& operator<< (Ncurses& left,int right)
{
	left.print(right);
	return left;
}


Ncurses& operator>> (Ncurses& left,std::string& right)
{
	right = left.getLine();
	return left;
}

Ncurses& operator>> (Ncurses& left,int& right)
{
	right = stoi(left.getLine(), nullptr);
	return left;
}

Ncurses& operator>> (Ncurses& left,char& right)
{
	right = left.getKey();
	return left;
}
