//***********************************************************************/
//***********************************************************************/
//****	Project: 	GEM Audio Player
//****	Programmer: Gabriel E. Marcano
//****	Oper. Sys.: Linux
//****	File Name:	main_Linux.cpp
//****	Date:			May 10, 2008
//***********************************************************************/
//***********************************************************************/
//***********************************************************************/

//***********************************************************************/
//***********************************************************************/
//****	TO DO:
//****
//****-Separate console output from inner functions of classes. This
//****	modularization will prepare the program for a GUI extension.
//****-Attend FIXMEs as they show up.
//**** |_> Urgent FIXME: Develop function to parse arguments passed or
//****			 develop some other way to get input.
//****-Figure out reason as to why code Seg Faults. Try to correct it!
//****-
//****
//***********************************************************************/
//***********************************************************************/

//***********************************************************************/
//***********************************************************************/
//****	ChangeLog
//****
//****	-Revision 0.0.1 not documented due to lack of organization. It was
//****	  a clone of Chris Lee's spcplay.cpp which used an older version
//****	  of Anti Resonance's SNESAPU.
//****
//**** March 21, 2008
//****	-Finished revision 0.0.2
//****	  Reorganized program into functions. One (LoadSPC) loads the SPC
//****	    file, the other (StartEmu) starts up the emulator and uses
//****	    LoadSPC to initialize the file.
//**** March 22, 2008
//****	-Made function_prototype.h and moved prototypes there. 3 prototypes.
//****	-Made constants.h: VERSION and Fade_Length moved there
//**** March 23, 2008
//****	-Added SAMPLING_HERTZ where variable is required (found in header)
//**** March 24, 2008
//****	-Deleted *pAPUBase and *pAPURAM. Old artifacts from old versions.
//**** April 28, 2008
//****	-SAMPLING_HERTZ removed for debugging of setAPUOpt()
//**** May 8, 2008
//****	-Added contEmuVariables.spcfadelen for fade length
//****	-Fixed/cleaned up setDSPlength()
//****	-Made LoadSPC() write length to container passed
//****	-contEmuVariables was removed and StartEmu and Load SPC were
//****		moved to a class, Emu_control.
//****	-Got rid of contLibao and made a Libao_control class.
//****		contains a bunch of generic functions.
//****	-Made pWAV global since it is accessed by both classes equally
//**** May 10, 2008
//****	-Due to classes updating, made playing easier. Very unmodular due
//****		to dependance on console and libao
//**** May 11, 2008
//****	-Fixed Error having to do with no file input for syntax help
//****		(console).
//**** June 22, 2008
//****	-Major changes to the structure of the program. New classes were
//****		added, makeWAV finished, some bugs in all of the other classes
//****		corrected.
//***********************************************************************/
//***********************************************************************/

#include <SnesapuEmulator.h>
#include <Libao_control.h>
#include <wave.h>
#include <Ncurses.h>
#include <console_disp.h>
#include <helper.h>

#include <csignal>
#include <string>
#include <chrono>
#include <thread>
#include <filesystem>

using namespace std::chrono_literals;

SnesapuEmulator emu_sys;
Libao_control libao;
WAVE wave;
Ncurses sys = Ncurses::get();
console_d console(sys, 0, 0);

std::sig_atomic_t end = false;

void main_menu(int)
{

	/*while (1)
	{
		system("clear");
		cout << "Main Menu\n\nSelect option:\n1. Continue/replay song\n2. Play new song\n3. Exit\nOption: ";
		int option;
		while(1)
		{
			cin >> option;
			if (cin.fail() == 1)
			{
				cin.clear();
				cin.ignore(INT_MAX,'\n');
}
if ((option != 1) && (option != 2) && (option != 3))
{
	cout << "Please choose a legit option.\n";continue;
}
break;
}
//if 1.

//if 3...
if (option ==3)
break;

//if 2.
if (option == 2)
{
	std::string file;
	cout << "Type in file location: ";
	while (1)
	{
		cin.ignore(INT_MAX,'\n');
		getline(cin,file);
		if (cin.fail() == 1)
		{
			cin.clear();
			cin.ignore(INT_MAX,'\n');
			cout << "Bad location...";
			continue;
}
break;
}
var_c.set_filename(file);
emu_sys.EmuConfig();

}*/

	end = true;
}

int main(int argc, char* argv[])
{
	// Testing changing dimensions...
	console.setdimension(50, 50);
	console.setdimension(60, 50);

	signal(SIGINT, main_menu);

	// Require a filename as an argument
	//FIXME: Parse the arguments first in a more efficient way than to just use what's returned. Make modular function?
	if (argc < 2) {
		sys << "usage: ./play <filename>\n";
		std::string g;
		sys >> g;
		return -1;
	}

	if (!std::filesystem::exists(argv[1]) || std::filesystem::is_directory(argv[1]))
	{
		sys << argv[1] << " is not a file!\n";
		sys << "usage: ./play <filename>\n";
		std::string g;
		sys >> g;
		return -1;
	}

	emu_sys.loadSPC(argv[1]);


	if (askYN("Play File(Y) or Make File(N)",sys) == true)
	{
		//Starts up the basic emulator and loads file given as command line argument
		// The real fun stuff. Play the song already.
		//FIXME: This poses a problem because I can't guarantee this case is always true.
		//FIXME: Make a member function that loads/verifies spcFilename

		//console.menu_init("File About");

		std::string buf = "GEM SNES Player Version "; buf += "1";
		console.print_line(buf, text_alignment::CENTER); //"GEM SNES Player Version "; buf += VERSION.c_str();
		//console.print_ends(1);
		console.print_blankline();
		const spc_tag *tag = emu_sys.get_spc_tag();
		buf = "Game: ";
		buf += spc_tag_get_game_title(tag);
		console.print_line(buf, text_alignment::CENTER);
		buf = "Song: ";
		buf += spc_tag_get_song_title(tag);
		console.print_line(buf, text_alignment::CENTER);
		console.time_place(spc_tag_get_seconds_until_fade(tag));
		console.percent_bar();
		console.print_blankline();
		console.message_init("Press Ctrl-C to enter menu or wait for song to end...", 3s);

		console.message_refresh();
		console.print();

		//meaning of the loop: i stands for seconds. While the second count is less than the length of the 	music played, emulate it.
		//The EmuAPU function writes one second worth of data to a buffer, pWAV, which ao_play then plays 	for one second.
		//During this second the loop restart.
		std::vector<int16_t> buff;
		for (uint32_t i = 0; !end && i < spc_tag_get_seconds_until_fade(tag); i++)
		{
			console.message_refresh();
			console.print();
			buff = emu_sys.EmulateOneSecond();
			libao.play(reinterpret_cast<char*>(buff.data()), buff.size()*2);
			console.time_reload(i+1,spc_tag_get_seconds_until_fade(tag));
			console.percent_bar_reload((i+1)*100/spc_tag_get_seconds_until_fade(tag));
			console.print();
		}
		sys << "\n\n";


	}

	else
	{
		/*
		std::string rootname = var_c.get_gamename(); rootname += " ]"; rootname += "[ "; rootname += var_c.get_songname();
		sys << "Making File...\n";
		short option;
		int end = 0;
		while (!end)
		{
			sys << "Make raw PCM file(1) or Wave sound file(2)?\n";
			sys >> option;
			if (cin.fail() == 1)
			{
				cin.clear();
				cin.ignore(INT_MAX,'\n');
			}
			if ((option != 1) && (option != 2) && (buffer != 3))
			{
				sys << "Please choose a legit option.\n";
			}
			else
			{
				if (option == 1)
				{
					sys << "Making raw PCM file as " << rootname << ".raw...   ";
					wave.makePCM(emu_sys);
				}
				if (option == 2)
				{
					sys << "Making Wave sound file as " << rootname << ".wav...   ";
					wave.makeWAV(emu_sys,libao);
				}
				end = 1;
			}
		}*/
		sys << "Done.\n";
	}
	return 0;
}
