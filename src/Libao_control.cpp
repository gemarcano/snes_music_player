//***********************************************************************/
//***********************************************************************/
//****	Project: 	GEM Audio Player
//****	Programmer:	Gabriel E. Marcano
//****	Oper. Sys.:	Linux
//****	File Name:	Libao_control.cpp
//****	Date:		June 24, 2008
//***********************************************************************/
//***********************************************************************/
//***********************************************************************/

//***********************************************************************/
//***********************************************************************/
//****	ChangeLog
//****
//**** June 24, 2008
//****	-Made this header.
//****
//****
//****
//****
//****
//***********************************************************************/
//***********************************************************************/

#include <Libao_control.h>

#include <ao/ao.h>
#include <cstdint>
#include <iostream>


//***********************************************************************/
//******************   Libao Functions   ********************************/
//***********************************************************************/

Libao_control::~Libao_control()
{
	ao_close(device);
	ao_shutdown();
}

Libao_control::Libao_control()
{
	// Set up libao
	ao_initialize();

	format.bits = 16;
	format.channels = 2; //Stereo
	format.rate = 32000;
	format.byte_format = AO_FMT_LITTLE;
	buf_size = ((format.bits)/8 * (format.channels) * (format.rate));
	device = ao_open_live(ao_default_driver_id(), &(format), NULL);
	if (device == NULL)
	{
		std::cerr << "Error opening device.\n";
		exit(0);
	}
}

bool Libao_control::play(const char *buf, size_t buf_size)
{
	ao_play(device, const_cast<char*>(buf), buf_size);
	return true;
}

bool Libao_control::play(const std::vector<unsigned char>& buf)
{
	play(reinterpret_cast<const char*>(buf.data()), buf.size());
	return true;
}

bool Libao_control::play(const std::vector<int16_t>& buf)
{
	play(reinterpret_cast<const char*>(buf.data()), 2*buf.size());
	return true;
}


uint32_t Libao_control::valbufsize()
{
	return buf_size;
}

int Libao_control::valformat_bit()
{
	return format.bits;
}

int Libao_control::valformat_ch()
{
	return format.channels;
}

int Libao_control::valformat_rate()
{
	return format.rate;
}

int Libao_control::set_ao_channels(int ch)
{
	format.channels = ch;
	return 0;
}

int Libao_control::set_ao_rate(int r)
{
	format.rate =r;
	return 0;
}

int Libao_control::set_ao_bits(int b)
{
	format.bits = b;
	return 0;
}

