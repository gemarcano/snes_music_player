//***********************************************************************/
//***********************************************************************/
//****	Project: 	GEM Audio Player
//****	Programmer:	Gabriel E. Marcano
//****	Oper. Sys.:	Linux
//****	File Name:	console_disp.cpp
//****	Date:		June 24, 2008
//***********************************************************************/
//***********************************************************************/
//***********************************************************************/

//***********************************************************************/
//***********************************************************************/
//****	ChangeLog
//****
//****
//***********************************************************************/
//***********************************************************************/

#include <console_disp.h>
#include <Ncurses.h>

#include <string>
#include <vector>
#include <chrono>

using namespace std::chrono_literals;

console_d::console_d(Ncurses& ncurses, short w, short h)
: sys(&ncurses), window_(w, h, {0, 0})
{
	//Size of the array
	percent_loc = window_.get_cursor();
	time_loc = percent_loc;
	mesg_loc = percent_loc;
	menu_loc = percent_loc;

	auto dims = window_.get_dimensions();
	width = dims.first;
	height = dims.second;
}

console_d::~console_d()
{
}

int console_d::print_line(std::string words, text_alignment align)
{
	return window_.print_line(words, align);
}

void console_d::setdimension(short w, short h)
{
	window_.set_dimensions(w, h);
	auto dims = window_.get_dimensions();
	width = dims.first;
	height = dims.second;

}

int console_d::print()
{
	//Refresh screen
	sys->draw();//clears screen, sets up for further calls
	window_.draw();

	return 0;
}

int console_d::print_blankline()
{
	return window_.print_blankline();
}

int console_d::percent_bar()
{
	int sides = width/10;
	int per_length = width - (2*sides);

	percent_loc = window_.get_cursor();
	std::string buffer;
	buffer.append(sides-1, ' ');
	buffer += "%0";
	buffer.append(per_length-6, ' ');
	buffer += "%100";
	buffer.append(sides-1, ' ');
	window_.print_line(percent_loc, buffer);
	buffer.clear();

	percent_loc.second += 1;
	buffer.append(sides-1, ' ');
	buffer += "|";
	buffer.append(per_length-2, ' ');
	buffer += "|";
	buffer.append(sides-1, ' ');
	window_.print_line(percent_loc, buffer);

	return 0;
}

int console_d::percent_bar_reload(double per)
{
	std::string buffer;
	int sides = width/10;
	int per_length = width - (2*sides);

	buffer.append(sides-1, ' ');
	buffer += "|";

	//Take care of updating the percent
	double dashes = per_length-2;
	double percent_add = 100/dashes;
	int result = (per+1)/percent_add;

	buffer.append(result, '-');
	buffer.append(per_length-2-result, ' ');
	buffer += "|";

	buffer.append(sides-1, ' ');

	window_.print_line(percent_loc, buffer);
	return 0;
}

int console_d::time_place(int total_time)
{
	size_t sides = width/10;
	size_t per_length = width - (2*sides);
	time_loc = window_.get_cursor();//Begin, start

	std::string temp = std::to_string(total_time);

	std::string buffer;
	buffer.append(sides-1, ' ');
	buffer += "Time: 0";
	buffer.append(per_length-7-11-temp.size(), ' ');
	buffer += "Time Left: ";
	buffer += temp;
	buffer.append(sides-1, ' ');
	window_.print_line(time_loc, buffer);

	return 0;
}

int console_d::time_reload(int time,int total_time)
{
	std::string buffer;

	size_t sides = width/10;
	size_t per_length = width - (2*sides);
	std::string temp1 = std::to_string(time);
	std::string temp2 = std::to_string(total_time-time);

	buffer.append(sides-1, ' ');
	buffer += "Time: ";
	buffer += temp1;
	buffer.append(per_length-6-temp1.size()-11-temp2.size(), ' ');
	buffer += "Time Left: ";
	buffer += temp2;
	buffer.append(sides-1, ' ');

	window_.print_line(time_loc, buffer);

	return 0;
}

//Mesage Sub-Section

int console_d::message_init(const std::string& msg, const std::chrono::seconds& seconds)
{
	mesg_loc = window_.get_cursor();
	window_.print_line(msg);
	mesg_timer = std::chrono::steady_clock::now() + seconds;

	return 0;
}

int console_d::message_refresh()
{
	// FIXME handle time logic, check if timer has run its course
	if (mesg_timer-0.5s <= std::chrono::steady_clock::now())
	{
		window_.set_cursor(mesg_loc);
		window_.print_blankline();
	}
	return 0;
}


/////////////////////////////////////////////////////////////////
//Menu Sub-section
/////////////////////////////////////////////////////////////////
int console_d::menu_init(std::string things)
{
	//cut up the things in things

	std::vector<std::string> buf(1);
	int str_index = 0;
	std::string buffer;

	for (size_t let_index = 0; let_index < things.size(); let_index++)
	{
		if (things[let_index] != ' ')
		{
			buf[str_index] += things[let_index];
		}
		else
		{
			buf.emplace_back();
		}
	}
	menu_loc = window_.get_cursor();//locate place of menu

	for (int i = 0; i < width; i++)
	{
		buffer += '-';
	}

	std::string menu_line;
	for (int i = 0; i <= str_index; i++)
	{
		menu_line += buf[i];
		menu_line += " |";
	}
	print_line(menu_line, text_alignment::LEFT);
	for (int i = 0; i < width-2; i++)
	{
		buffer += '-';
	}
	buffer += '\n';
	return 0;
}
