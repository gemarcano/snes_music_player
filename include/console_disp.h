#ifndef CONSOLE_D
#define CONSOLE_D

#include <Ncurses.h>
#include <string>
#include <vector>
#include <chrono>
#include <array>


template<class T>
void console_draw(T& widget)
{
	widget.draw();
}

enum class text_alignment
{
	LEFT,
	CENTER,
	RIGHT
};

class console_window
{
public:
	console_window(short width, short height, const std::pair<int, int>& coordinate)
	:console_window(width, height, coordinate, {'_', '-', '|', '|'})
	{}

	console_window(short width, short height, const std::pair<int, int>& coordinate, const std::array<char, 4>& border)
	:border_(border)
	{
		Ncurses& ncurses = Ncurses::get();
		curs_set(0);
		(void)ncurses;
		border_window_ = newwin(height, width, coordinate.second, coordinate.first);
		// Need to grab height and width, since they could be 0 to indicate full screen
		getmaxyx(border_window_, height, width);
		window_ = derwin(border_window_, height-2, width-2, 1, 1);
		wborder(border_window_, border[2], border[3], border[0], border[1], border[2], border[2], border[3], border[3]);
	}

	~console_window()
	{
		delwin(window_);
		werase(border_window_);
		delwin(border_window_);
	}

	void draw()
	{
		touchwin(border_window_);
		wrefresh(window_);
		wrefresh(border_window_);
	}

	//Generic function used to print a line in code. align is used to determine where text is located. 1 = left, 2 center, 3 right
	int print_line(std::pair<int, int> coordinate, const std::string& words, text_alignment align = text_alignment::LEFT)
	{
		size_t size = words.length();
		//determine how much space of the alloted width it takes up
		// FIXME what if negative?
		int width, height;
		getmaxyx(window_, height, width);
		(void)height;
		int space_pad = width - size;
		// FIXME this isn't quite right for center alignment. The last overflow
		// line needs padding, as does the right alignment
		if (space_pad < 0)
			space_pad = 0;

		std::string buffer;
		switch (align)
		{
		case text_alignment::LEFT:
			buffer += words;
			buffer.append(space_pad, ' ');
			break;
		case text_alignment::CENTER:
			buffer.append(space_pad/2, ' ');
			buffer += words;
			buffer.append(space_pad - space_pad/2, ' ');
			break;
		case text_alignment::RIGHT:
			buffer.append(space_pad, ' ');
			buffer += words;
		}
		// Don't need to print '\n' as the buffer should exactly be the width
		// of the window, causing ncurses to wrap around
		mvwprintw(window_, coordinate.second, coordinate.first, buffer.c_str());
		return 0;
	}

	int print_line(const std::string& words, text_alignment align = text_alignment::LEFT)
	{
		int x, y;
		getyx(window_, y, x);
		return print_line({0,y}, words, align);
	}

	int print_blankline()
	{
		return print_line("");
	}

	void set_cursor(const std::pair<int, int>& coordinate)
	{
		wmove(window_, coordinate.second, coordinate.first);
	}

	std::pair<int, int> get_cursor()
	{
		int x, y;
		getyx(window_, y, x);
		return {x, y};
	}

	std::pair<int, int> get_dimensions()
	{
		int width, height;
		getmaxyx(window_, height, width);
		return {width, height};
	}

	// FIXME need to redraw everything
	void set_dimensions(short width, short height)
	{
		wresize(border_window_, height, width);
		wresize(window_, height-2, width-2);
		wborder(border_window_, border_[2], border_[3], border_[0], border_[1], border_[2], border_[2], border_[3], border_[3]);
	}

private:
	std::array<char, 4> border_;
	WINDOW *border_window_;
	WINDOW *window_;

};

class console_d//Base console display class. Contains generic help functions
{
public:
	console_d(Ncurses& ncurses, short w, short h);
	~console_d();

	int print(); //Draws buffer.
	void setdimension(short w, short h); //Sets dimenstion variables
	int print_line(std::string words, text_alignment align = text_alignment::LEFT); //Generic function used to print a line in code. align is used to determine where text is located. 1 = left, 2 center, 3 right
	int print_blankline();

	/*some function to print error codes?*/

	/*Generic Display functions*/
public:
	int percent_bar();
	int percent_bar_reload(double per);
public:
	int time_place(int total_time);
	int time_reload(int time,int total_time);
public:
	int message_init(const std::string& msg, const std::chrono::seconds& seconds = std::chrono::seconds(3));
	int message_refresh();
public:
	int menu_init(std::string);


private:
	//console_io con_io;
	Ncurses* sys;
	console_window window_;

private:
	short width, height; //Width and height of program in display. // Width is length of line minus '\n'

private:
	std::pair<int, int> percent_loc;		//location of percent bar in buffer

private:
	std::pair<int, int> time_loc;			//Location where time line is located

private:
	std::chrono::steady_clock::time_point mesg_timer;		//Timer for the message system;
	std::pair<int, int> mesg_loc;

private:
	//menu sub-section
	std::string menu_buffer;	//pointer to buffer array
	std::pair<int, int> menu_loc;


};

#endif //CONSOLE_D
