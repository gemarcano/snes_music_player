#ifndef NCURSES_H_
#define NCURSES_H_

#include <ncurses.h>
#define BACKSPACE 127
#include <vector>
#include <string>
#include <sstream>
#include <memory>

/**************************************
 *basic outline of a ncurses driver class:
 *constructor to set up the terminal for ncurses
 *-perhaps separate objects consisting of input and graphic display?
 *-destructor to clean up terminal
 **************************************/

class nwindow
{
public:
	nwindow(int rows, int cols, int x, int y)
	{
		window_ = newwin(rows, cols, y, x);
	}

	~nwindow()
	{
		delwin(window_);
	}

	//*************************************
	//****	Printing Functions
	//*************************************
	template<class T>
	void print(T&& s)
	{
		std::stringstream ss;
		ss << std::forward<T>(s);
		wprintw(window_, "%s", ss.str().c_str());
	}

	template<class T>
	void println(T&& s)
	{
		print(std::forward<T>(s));
		wprintw(window_, "\n");
	}

	//*************************************
	//****	Formatting Activation Functions
	//*************************************
	void switchUnderline()
	{
		short pair[2];
		attr_t attr;
		wattr_get(window_, &attr, pair, nullptr);
		if (attr & A_UNDERLINE)
			wattroff(window_, A_UNDERLINE);
		else
			wattron(window_, A_UNDERLINE);
	}

	void switchBold()
	{
		short pair[2];
		attr_t attr;
		wattr_get(window_, &attr, pair, nullptr);
		if (attr & A_UNDERLINE)
			wattroff(window_, A_BOLD);
		else
			wattron(window_, A_BOLD);

	}
//		void switch();
//		void switch();
	//void setColor(int);

	//************************************
	//****	Informative functions
	//************************************
	std::pair<int, int> get_cursor()
	{
		int y, x;
		getyx(window_, y, x);
		return {x, y};
	}

	//************************************
	//****	Window related functions
	//************************************
	void draw()
	{
		wrefresh(window_);
		wmove(window_, 0, 0);
	}

	//************************************
	//****	Input Related Functions
	//************************************
	std::string getLine();
	int getKey();

private:
	WINDOW *window_;
};

class Ncurses
{
	private:

		//*************************************
		//****	WINDOW related information
		//*************************************
		std::vector<nwindow> windows; //pointer to an array of pointers windows, array[0] being stdscr. start size is 4.
		int activeWin; //number of active windows

		//Color and Attribute information
		//color=?
		bool bold, underline;	//status of attributes. To be added more

		Ncurses();

	public:
		static Ncurses& get();
		~Ncurses();//releases resourses used by ncurses

		//*************************************
		//****	Printing Functions
		//*************************************
		void print(int s);
		void print(std::string s);
		void print(const char* s);
		void print(char s);

		void println(std::string s);
		void println(const char* s);
		void println(char s);

		//*************************************
		//****	Formatting Activation Functions
		//*************************************
		void switchUnderline();
		void switchBold();
//		void switch();
//		void switch();
		//void setColor(int);

		//************************************
		//****	Informative functions
		//************************************
		int getX(int w = 0);
		int getY(int w = 0);

		//************************************
		//****	Window related functions
		//************************************
		void draw();//all by default
		nwindow& addWindow(int height, int width, int startx, int starty);
		void closeWindow(size_t w);

		//************************************
		//****	Input Related Functions
		//************************************
		std::string getLine();
		int getKey();

		template <class Type>
		void prompt(std::string varname, Type& var)
		{
			//cout << "Input value for " << varname << ":" << "\n";
			print("Input value for ");
			print(varname);
			print(":\n");;
			std::string buffer;
			//cin >> buffer;
			std::stringstream ss;
			ss << buffer;
			Type b;
			ss >> b;
			if (b == 0) return;
			var = b;

		/*	if (cin.fail() == 1)
			{
				cin.clear();
				cin.ignore(INT_MAX,'\n');
			}*/
		}
};

/*Printing functions (make Ncurses behave like cout)
*
*/

Ncurses& operator << (Ncurses& left, std::string& right);
Ncurses& operator << (Ncurses& left, const char* right);
Ncurses& operator << (Ncurses& left, char right);
Ncurses& operator << (Ncurses& left, int right);

Ncurses& operator >> (Ncurses& left, std::string& right);
Ncurses& operator > (Ncurses& left, int& right);
Ncurses& operator > (Ncurses& left, char& right);

#endif//NCURSES_H_
