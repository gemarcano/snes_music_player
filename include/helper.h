#ifndef HELPER_H_
#define HELPER_H_

#include <Ncurses.h>
#include <iostream>
#include <string>
#include <climits>

//helper function

bool askYN(std::string mesg, Ncurses& sys);
bool askYN(Ncurses& sys);

//helper 2
template <class Type>
void prompt(std::string varname, Type& var)
{
	std::cout << "Input value for " << varname << ":" << "\n";
	std::string buffer;
	std::cin >> buffer;
	int b = stoi(buffer, nullptr);
	if (b == 0) return;
	var = b;

	if (std::cin.fail() == 1)
	{
		std::cin.clear();
		std::cin.ignore(INT_MAX,'\n');
	}

}

#endif//HELPER_H_
