#ifndef LIBAO_C
#define LIBAO_C

#include <ao/ao.h>
#include <cstdint>
#include <vector>

///-----------------------------------------------------------------------
///Class Libao_control
///	This class controls functions having to do with Libao playing and
///	output
///-----------------------------------------------------------------------
class Libao_control
{
//private:
private:
	///Size of the buffer libao will be reading
	uint32_t buf_size;
	///pointer to audio device
	ao_device *device;
	///format for sound to be played in (specs in Libao docs)
	ao_sample_format format;
public:
	///Default constructor. Initializes format and opens and readies device
	Libao_control();
	///Closes device
	~Libao_control();
	///Returns true if successful. Plays wav from defined buffer (void*)
	bool play(const char *buf, size_t buf_size);
	bool play(const std::vector<unsigned char>& buf);
	bool play(const std::vector<int16_t>& buf);

	uint32_t valbufsize();

	int valformat_bit();
	int valformat_ch();
	int valformat_rate();

	int set_ao_channels(int);
	int set_ao_rate(int);
	int set_ao_bits(int);
};

#endif //LIBAO_C
