#ifndef SNESAPU_EMU_IMPL_H
#define SNESAPU_EMU_IMPL_H

#include <snes_spc/spc.h>
#include <spc_tag/spc_tag.h>

#include <vector>
#include <string>

///-----------------------------------------------------------------------
///Class SnesapuEmulator
///	This class controls functions having to do with the Emulator and its
///	control, setup, and rendering.
///-----------------------------------------------------------------------
class SnesapuEmulator
{
	private:
		SNES_SPC *emu;
		spc_tag *spc;

		//Speed of the SPC. ratio. Default 1.
		double mSpcSpeed;
		//Pitch in hz values. 32000 is default
		unsigned long mSpcPitch;
		//Used to set the length of fading;
		unsigned long mSpcFadeLength;

	public:

		SnesapuEmulator();
		~SnesapuEmulator();
		std::vector<int16_t> EmulateOneSecond();

		int loadSPC(const std::string);

		const spc_tag *get_spc_tag() const;

		unsigned long getPlayPitch();
		double getPlaySpeed();
		unsigned long getPlayFadeLength();
};

#endif //SNESAPU_EMU_IMPL_H
