#include <QWidget>
#include <QPushButton>
#include <QGridLayout>
#include <QApplication>
#include <QtGui>

#include <thread>

class GUI_TEST : public QWidget
{
	Q_OBJECT

	private:
		QPushButton* playButton;
		QPushButton* stopButton;
		std::thread audio_thread;

	public:
		GUI_TEST(QWidget* parent = 0);

	public slots:
		void play();
		void stop();

//	signals:
//		void sPlay();
//		void sStop();
};
