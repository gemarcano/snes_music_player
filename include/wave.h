//WAVE Class header

#ifndef WAVE_C
#define WAVE_C

#include <string>
#include <cstdint>
#include <SnesapuEmulator.h>
#include <Libao_control.h>

class WAVE
{
private:
	std::string riff;
	int32_t ChunkSize;
	std::string format;
	std::string SubChunk1ID;
	int32_t Subchunk1Size;
	int16_t AudioFormat;
	int16_t NumChannels;
	int32_t SampleRate;
	int32_t ByteRate;
	int16_t BlockAlign;
	int16_t BitsPerSample; //8,16,32,64,...
	std::string Subchunk2ID;
	int32_t Subchunk2size;

public:
	int makePCM(SnesapuEmulator& Emu);
	int makeWAV(SnesapuEmulator& Emu, Libao_control& Lib);
	WAVE();
};

#endif //WAVE_C
